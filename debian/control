Source: airstrike
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Barry deFreese <bdefreese@debian.org>,
 Markus Koschany <apo@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libsdl-image1.2-dev,
 libsdl-mixer1.2-dev
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/games-team/airstrike.git
Vcs-Browser: https://salsa.debian.org/games-team/airstrike
Homepage: https://icculus.org/airstrike/

Package: airstrike
Architecture: any
Depends:
 airstrike-common,
 ${misc:Depends},
 ${shlibs:Depends}
Description: 2d dogfight game in the tradition of 'Biplanes' and 'BIP'
 Airstrike is a 2d dogfight game in the tradition of the Intellivision and
 Amiga games 'Biplanes' and 'BIP'. It features a robust physics engine and
 several other extensions of the original games. Playable by
 1 or 2 persons.

Package: airstrike-common
Architecture: all
Depends:
 ${misc:Depends}
Multi-Arch: foreign
Description: 2d dogfight game - data files
 Airstrike is a 2d dogfight game in the tradition of the Intellivision and
 Amiga games 'Biplanes' and 'BIP'. It features a robust physics engine and
 several other extensions of the original games. It is currently 0-2 player
 only, but will hopefully have network play and some more advanced computer
 controlled enemies in the future. The graphics have been created using the
 Povray raytracer, and should be easy to extend and modify.
 .
 This package contains the architecture-independent data files.
